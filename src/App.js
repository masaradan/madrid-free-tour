import express from "express";
import http from "http";

import { mainRouter } from "./common/modules/MainRouter";

class App {
  express;
  server;

  constructor() {
    this.init();
  }

  start() {
    try {
      this.server.on("error", this.onServerError.bind(this));

      this.server.on("listening", this.onServerListening.bind(this));

      return this.server.listen(3000);
    } catch (err) {
      process.exit(1);
    }
  }

  init() {
    this.configExpress();

    this.configServer();
  }

  configExpress() {
    this.express = express();

    this.express.use(mainRouter.middleware);
  }

  configServer() {
    this.server = http.createServer(this.express);
  }

  onServerError(err) {
    console.log("down " + err);
  }

  onServerListening() {
    const address = this.server.address();

    console.log("Listening on " + address.port);
  }
}

export const app = new App();
