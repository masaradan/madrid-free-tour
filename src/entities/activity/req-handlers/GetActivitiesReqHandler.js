import { findActivitiesInteractor } from "../interactors/FindActivitiesInteractor";
import { ActivityFindQuery } from "../models/domain/ActivityFindQuery";
import { activityToActivityApiAdapter } from "../adapters/ActivityToActivityApiAdapter";
import { FeatureCollection } from "../../../common/models/FeatureCollection";

class GetActivitiesReqHandler {
  async handle(req) {
    const activityFindQuery = new ActivityFindQuery();

    activityFindQuery.category = req.query.category ?? undefined;
    activityFindQuery.location = req.query.location ?? undefined;
    activityFindQuery.district = req.query.district ?? undefined;

    const activities = await findActivitiesInteractor.interact(
      activityFindQuery
    );

    const activitiesApi = activities.map(activity =>
      activityToActivityApiAdapter.adapt(activity)
    );

    const featureCollection = new FeatureCollection(activitiesApi);

    return featureCollection;
  }
}

export const getActivitiesReqHandler = new GetActivitiesReqHandler();
