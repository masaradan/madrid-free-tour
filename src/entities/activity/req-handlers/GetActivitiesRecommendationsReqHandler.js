import { findRecommendedActivityInteractor } from "../interactors/FindRecommendedActivityInteractor";
import { ActivityFindQuery } from "../models/domain/ActivityFindQuery";
import { activityToActivityApiAdapter } from "../adapters/ActivityToActivityApiAdapter";

class GetActivitiesRecommendationsReqHandler {
  async handle(req) {
    if (req.query.availabilityTimeEnd && req.query.availabilityTimeStart) {
      const activityFindQuery = new ActivityFindQuery();

      activityFindQuery.category = req.query.category ?? undefined;
      activityFindQuery.availabilityDay = req.query.week_day ?? undefined;
      activityFindQuery.availabilityTimeStart =
        req.query.time_start ?? undefined;
      activityFindQuery.availabilityTimeEnd = req.query.time_end ?? undefined;

      activityFindQuery.isRecommendation = true;

      const activity = await findRecommendedActivityInteractor.interact(
        activityFindQuery
      );

      const activityApi = activityToActivityApiAdapter.adapt(activity);

      return activityApi;
    } else {
      const error = new Error();
      error.message = "Both time range properties required";
      error.code = 400;

      throw error;
    }
  }
}

export const getActivitiesRecommendationsReqHandler = new GetActivitiesRecommendationsReqHandler();
