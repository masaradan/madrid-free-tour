export class ActivityApi {
  type = "Feature";
  geometry = {
    type: "Point",
    coordinates: []
  };
  properties = {
    name: "",
    opening_hours: {},
    hours_spent: 0,
    category: "",
    location: "",
    district: ""
  };

  constructor(
    coordinates,
    name,
    opening_hours,
    hours_spent,
    category,
    location,
    district
  ) {
    this.geometry.coordinates = coordinates;
    this.properties.name = name;
    this.properties.opening_hours = opening_hours;
    this.properties.hours_spent = hours_spent;
    this.properties.category = category;
    this.properties.location = location;
    this.properties.district = district;
  }
}
