export class ActivityFindQuery {
  constructor(
    category,
    location,
    district,
    availabilityTimeStart,
    availabilityTimeEnd,
    availabilityDay,
    isRecommendation
  ) {
    this.category = category, 
    this.location = location, 
    this.district = district, 
    this.availabilityTimeStart = availabilityTimeStart;
    this.availabilityTimeEnd = availabilityTimeEnd;
    this.availabilityDay = availabilityDay;
    this.isRecommendation = isRecommendation;
  }
}
