import fs from "fs";
import path from "path";

import { parseTimeStringToInt } from "../../../common/utils/parseUtils";

class ActivityRepository {
  dataPath = path.join(__dirname, "../../../common/madrid.json");
  activities;

  constructor() {
    this.createConnection();
  }

  createConnection() {
    let dataFile = fs.readFileSync(this.dataPath, "utf8");
    const rawActivities = JSON.parse(dataFile);

    this.activities = rawActivities.map(activity => {
      const opening_hours_array = [];

      for (const day in activity.opening_hours) {
        if (activity.opening_hours.hasOwnProperty(day)) {
          const element = activity.opening_hours[day];
          if (element[0]) {
            opening_hours_array.push(element[0]);
          }
        }
      }

      activity.opening_hours_array = opening_hours_array;

      return activity;
    });
  }

  async find(query) {
    const filteredActivities = this.activities.filter(activity => {
      const isCategory = this.isValidCategory(query, activity);

      const isLocation = this.isValidLocation(query, activity);

      const isDistrict = this.isValidDistrict(query, activity);

      const isRecommendation = this.isValidRecommendation(query, activity);

      const isFiltered =
        isCategory && isLocation && isDistrict && isRecommendation;

      return isFiltered;
    });

    return filteredActivities;
  }

  isValidCategory(query, activity) {
    return query.category ? activity.category == query.category : true;
  }

  isValidLocation(query, activity) {
    return query.location ? activity.location == query.location : true;
  }

  isValidDistrict(query, activity) {
    return query.district ? activity.district == query.district : true;
  }

  isValidRecommendation(query, activity) {
    return query.isRecommendation
      ? this.isValidAvailability(query, activity)
      : true;
  }

  isValidAvailability(query, activity) {
    const hasAvailableTime =
      query.availabilityTimeStart && query.availabilityTimeEnd
        ? this.hasAvailableTime(query, activity)
        : false;

    return hasAvailableTime;
  }

  hasAvailableTime(query, activity) {
    const queryTimeStartInt = query.availabilityTimeStart
      ? parseTimeStringToInt(query.availabilityTimeStart)
      : undefined;

    const queryTimeEndInt = query.availabilityTimeEnd
      ? parseTimeStringToInt(query.availabilityTimeEnd)
      : undefined;

    const visitDuration = activity.hours_spent * 60;

    let isAvailableTime;

    if (query.availabilityDay) {
      const timeString = activity.opening_hours[query.availabilityDay][0];

      if (timeString) {
        isAvailableTime = this.hasLongerTimeThanVisitDuration(
          timeString,
          visitDuration,
          queryTimeStartInt,
          queryTimeEndInt
        );
      } else {
        isAvailableTime = false;
      }
    } else {
      for (let i = 0; i < activity.opening_hours_array.length; i++) {
        const timeString = activity.opening_hours_array[i];

        isAvailableTime = this.hasLongerTimeThanVisitDuration(
          timeString,
          visitDuration,
          queryTimeStartInt,
          queryTimeEndInt
        );

        if (isAvailableTime === true) {
          break;
        }
      }
    }

    return isAvailableTime;
  }

  hasLongerTimeThanVisitDuration(
    timeString,
    visitDuration,
    queryTimeStartInt,
    queryTimeEndInt
  ) {
    const indexOfDash = timeString.indexOf("-");

    const openingTime = timeString.slice(0, indexOfDash);
    const openingTimeInt = parseTimeStringToInt(openingTime);
    const closingTime = timeString.slice(indexOfDash + 1);
    const closingTimeInt = parseTimeStringToInt(closingTime);

    const hasLongerTimeThanVisitDuration =
      visitDuration <=
      (queryTimeEndInt > closingTimeInt ? closingTimeInt : queryTimeEndInt) -
        (queryTimeStartInt < openingTimeInt
          ? openingTimeInt
          : queryTimeStartInt);

    return hasLongerTimeThanVisitDuration;
  }
}

export const activityRepository = new ActivityRepository();
