import { Router } from "express";

import { getActivitiesRecommendationsReqHandler } from "../../req-handlers/GetActivitiesRecommendationsReqHandler";

class ActivitiesRecommendationsRouter {
  router;

  constructor() {
    this.router = new Router();

    this.init();
  }

  get middleware() {
    return this.router;
  }

  init() {
    this.router.route("/").get(this.get.bind(this));
  }

  async get(req, res, next) {
    try {
      const featureCollection = await getActivitiesRecommendationsReqHandler.handle(
        req
      );

      res.status(200);
      res.json(featureCollection);
    } catch (error) {
      res.status(error.code);
      res.json(error);
    }
  }
}

export const activitiesRecommendationsRouter = new ActivitiesRecommendationsRouter();
