import { Router } from "express";

import { activitiesRecommendationsRouter } from "./namespaces/ActivitiesRecommendationsRouter";
import { getActivitiesReqHandler } from "../req-handlers/GetActivitiesReqHandler";

class ActivitiesRouter {
  router;

  constructor() {
    this.router = new Router();

    this.init();
  }

  get middleware() {
    return this.router;
  }

  init() {
    this.router.route("/").get(this.get.bind(this));
    this.router.use(
      "/recommendations",
      activitiesRecommendationsRouter.middleware
    );
  }

  async get(req, res, next) {
    try {
      const featureCollection = await getActivitiesReqHandler.handle(req);

      res.status(200);
      res.json(featureCollection);
    } catch (error) {
      res.status(error.code);
      res.json(error);
    }
  }
}

export const activitiesRouter = new ActivitiesRouter();
