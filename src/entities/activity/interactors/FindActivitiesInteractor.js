import { activityRepository } from "../repositories/ActivityRepository";

class FindActivitiesInteractor {
  async interact(query) {
    const activities = await activityRepository.find(query);

    return activities;
  }
}

export const findActivitiesInteractor = new FindActivitiesInteractor();
