import { findActivitiesInteractor } from "./FindActivitiesInteractor";

class FindRecommendedActivityInteractor {
  async interact(query) {
    const activities = await findActivitiesInteractor.interact(query);

    const sortedActivities = activities.sort(
      (a, b) => b.hours_spent - a.hours_spent
    );

    return sortedActivities[0];
  }
}

export const findRecommendedActivityInteractor = new FindRecommendedActivityInteractor();
