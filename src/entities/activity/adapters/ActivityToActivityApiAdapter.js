import { ActivityApi } from "../models/api/ActivityApi";

class ActivityToActivityApiAdapter {
  adapt(activity) {
    const coordinates = [activity.latlng[1], activity.latlng[0]];

    const activityApi = new ActivityApi(
      coordinates,
      activity.name,
      activity.opening_hours,
      activity.hours_spent,
      activity.category,
      activity.location,
      activity.district
    );

    return activityApi;
  }
}

export const activityToActivityApiAdapter = new ActivityToActivityApiAdapter();
