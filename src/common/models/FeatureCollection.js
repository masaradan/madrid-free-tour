export class FeatureCollection {
  type = "FeatureCollection";
  features = [];
  total = 0;

  constructor(features) {
    this.features = features;
    this.total = features.length;
  }
}
