export const parseTimeStringToInt = timeString => {
  const indexOfColon = timeString.indexOf(":");

  const hour = timeString.slice(0, indexOfColon);
  const hourInt = parseInt(hour, 10);
  const minutes = timeString.slice(indexOfColon + 1);
  const minutesInt = parseInt(minutes, 10);

  const timeInt = hourInt * 60 + minutesInt;

  return timeInt;
};
