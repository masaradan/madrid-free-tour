import { Router } from "express";

import { activitiesRouter } from "../../entities/activity/routers/ActivitiesRouter";

class MainRouter {
  router;

  constructor() {
    this.router = new Router();

    this.init();
  }

  get middleware() {
    return this.router;
  }

  init() {
    this.router.use("/activities", activitiesRouter.middleware);
  }
}

export const mainRouter = new MainRouter();
